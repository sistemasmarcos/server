const mongoose = require('mongoose');
const transaction = require('./transaction');

const schema = new mongoose.Schema({
    login: {
      type: String,
      required: true,
      unique: true,
      index: true
    },
    senha: {
      type: String,
      required: true
    },
    nome: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    telefone: {
      type: String,
      required: true
    },
    transactions: {
      type: [transaction],
      required: true
    }
});

module.exports = mongoose.model('usuarios', schema);
