const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    razaoSocial: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    nomeFantasia: {
        type: String,
        required: true
    },
    cnpj: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: false
    },
    site: {
        type: String,
        required: false
    },
    imu: {
        type: String,
        required: true
    },
    cnae: {
        type: String,
        required: true
    },
    cep: {
        type: String,
        required: true
    },
    grauDeRisco: {
        type: String,
        required: true
    },
    endereco: {
        type: String,
        required: true
    },
    numero: {
        type: Number,
        required: true
    },
    complemento: {
        type: String,
        required: false
    },
    caixaPostal: {
        type: String,
        required: false
    },
    bairro: {
        type: String,
        required: true
    },
    cidade: {
        type: String,
        required: true
    },
    estado: {
        type: String,
        required: true
    },
    telefone1: {
        type: String,
        required: true,
    },
    telefone2: {
        type: String,
        required: false
    },
    contato1: {
        type: String,
        required: false,
    },
    contato2: {
        type: String,
        required: false,
    },
    categoria: {
        type: String,
        required: false,
    },
    data: {
        type: Date,
        required: true
    },
    dataContrato: {
        type: Date,
        required: true
    },
    dataVencimento: {
        type: Date,
        required: true
    },
    cipa: {
        type: Boolean,
        default: false
    },
    ppra: {
        type: Boolean,
        default: false
    },
    pcmso: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('empresas', schema);
