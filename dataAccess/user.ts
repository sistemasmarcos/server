import r from "../db";
import User from "../model/User";
import * as jsonwebtoken from "jsonwebtoken";

const TABLE_NAME = "users";
const tb = r.table(TABLE_NAME);

const getUser = (user: any) => {
  return tb.filter(user).run();
};

const getUsers = async () => {
  return await tb.run();
};

const insertUser = async (user: User) => {
  const sameLogin = await tb.filter({ login: user.login }).run();
  if (sameLogin.length > 0) {
    return null;
  }
  user.transactions = [];
  user.userData = {
    setores: [],
    cargos: [],
    cbos: [],
    cnaes: [],
    empresas: []
  };
  user.autoIncrements = {
    setores: 0,
    cargos: 0,
    cbos: 0,
    cnaes: 0,
    empresas: 0
  };
  return await tb.insert(user).run();
};

const deleteUser = async (user: any) => {
  return await tb
    .filter(user)
    .delete()
    .run();
};

const getUserById = async (id: string) => {
  try {
    return (await tb.filter({ id }).run()[0]) as User;
  } catch {
    return null;
  }
};

const login = async (login: string, password: string) => {
  try {
    const user = (await tb
      .filter({ login, password })
      .nth(0)
      .run()) as User;
    return jsonwebtoken.sign(
      {
        userId: user.id
      },
      "marcosPPRA123",
      { expiresIn: "1y" }
    );
  } catch {
    return null;
  }
};

type RegisterInput = {
  login: string;
  password: string;
  name: string;
  email: string;
  phone: string;
};
const register = async (user: User) => {
  if (await insertUser(user)) {
    return jsonwebtoken.sign(
      {
        userId: user.id
      },
      "marcosPPRA123",
      { expiresIn: "1y" }
    );
  }
  return null;
};

export {
  getUser,
  getUsers,
  insertUser,
  deleteUser,
  getUserById,
  login,
  register
};
