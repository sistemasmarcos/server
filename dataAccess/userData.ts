import r from "../db";
import * as users from "./user";

const tb = r.table("users");

const getCurrentIndex = async (userId: string, dataType: string) => {
  const user = await tb.get(userId).run();
  return user.autoIncrements[dataType] as number;
};

const getAll = async (userId: string, dataType: string) => {
  const user = await tb.get(userId).run();
  return user.userData[dataType];
};

const add = async (userId: string, dataType: string, payload: any) => {
  const index = await getCurrentIndex(userId, dataType);
  return (
    (await tb
      .get(userId)
      .update(doc => {
        return {
          autoIncrements: {
            [dataType]: index + 1
          },
          userData: {
            [dataType]: doc("userData")(dataType).append({
              ...payload,
              id: index + 1
            })
          }
        };
      })
      .run()).replaced === 1
  );
};

const update = async (userId: string, dataType: string, payload: any) => {
  return (
    (await tb
      .get(userId)
      .update(doc => {
        return doc.merge({
          userData: {
            [dataType]: doc("userData")(dataType)
              .filter(d => d("id").ne(payload.id))
              .append(payload)
          }
        });
      })
      .run()).replaced === 1
  );
};

const remove = async (userId: string, dataType: string, id: number) => {
  //console.log("remove: dataType: ", dataType, "id: ", id);
  const result = await tb
    .get(userId)
    .update(doc =>
      doc.merge({
        userData: {
          [dataType]: doc("userData")(dataType).filter(d => d("id").ne(id))
        }
      })
    )
    .run();
  //console.log("userData remove: ", result);
  return result.replaced === 1;
};

export { getCurrentIndex, getAll, add, update, remove };
