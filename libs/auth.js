const jwt = require('jsonwebtoken');
const servKey = require('../config').servKey;

exports.decodeToken = (token) => 
{
  return new Promise((resolve, reject) =>
  {
    jwt.verify(token, servKey, (err, decodedToken) => 
    {
      if (err || !decodedToken)
      {
        return reject(err)
      }

      resolve(decodedToken)
    })
  })
};

exports.generateToken = (userId) => {
  const token = jwt.sign({userId: userId, creationDate: new Date()}, servKey, {
    expiresIn: 3600,
    algorithm: 'HS256'
  });
  return token;
};