const express = require('express');
const router = express.Router({mergeParams: true});
const controller = require('../controller/auth');

router.post('/login', (req, res, next) => controller.login(req, res, next));
router.post('/register', (req, res, next) => controller.register(req, res, next));

module.exports = router;
