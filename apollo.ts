import { GraphQLServer } from "graphql-yoga";
import typeDefs from "./graphql/allTypes";
import resolvers from "./graphql/resolvers";
import * as cors from "cors";
const jwt = require("express-jwt");

const server = new GraphQLServer({
  typeDefs,
  resolvers,
  context: ({ request }) => ({
    user: (request as any).user
  })
});

server.use(cors());
server.use(jwt({ secret: "marcosPPRA123", credentialsRequired: false }));

server
  .start({ port: 8085 })
  .then(() => console.log("Running on http://localhost://8085"));
