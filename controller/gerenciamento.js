const mongoose = require('mongoose');

const requireModel = (table) => {
  try {
    const model = require('../model/' + table);
    return model;
  } catch(error) {
    return null;
  }
}

exports.get = (req, res, next) => {
  const model = requireModel(req.params.table);
  if (model === null) {
    res.sendStatus(404);
    return;
  }
  model.find({}, (err, doc) => {
    if (err) {
      res.sendStatus(404); // bad request
      return;
    }
    res.json(doc);
  });
};

exports.post = (req, res, next) => {
  const model = requireModel(req.params.table);
  if (model === null) {
    res.sendStatus(404);
    return;
  }
  const newItem = new model(req.body);
  newItem.save()
    .then(() => {
      res.sendStatus(201); // created
    })
    .catch(() => {
      res.sendStatus(409); // conflict (already exists)
    });
}

exports.patch = (req, res, next) => {
  const model = requireModel(req.params.table);
  if (model === null) {
    res.sendStatus(404);
    return;
  }
  model.findById(req.body._id, (err, existing) => {
    if (err || !existing) {
      res.sendStatus(404); // not found
      return;
    }
    existing.set(req.body);
    existing.save()
      .then(() => {
        res.sendStatus(200); // ok
      })
      .catch(() => {
        res.sendStatus(304); // Not modified
      });
  });
}

exports.delete = (req, res, next) => {
  const model = requireModel(req.params.table);
  if (model === null) {
    res.sendStatus(404);
    return;
  }
  const id = req.params.id;
  model.findById(id, (err, item) => {
    if (err || !item) {
      res.sendStatus(404); // not found
      return;
    }
    model.deleteOne(item, err => {
      if (err) {
        res.sendStatus(404); // not found
        return;
      }
      res.sendStatus(200); // ok
    })
  });
}