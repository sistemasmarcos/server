import { r, RDatum } from "rethinkdb-ts";
import { Connection } from "rethinkdb-ts";

const database = "marcos";
let connection: Connection;

interface Index {
  name: string;
  field: RDatum<any>;
}

interface Table {
  name: string;
  indexes: Index[];
}

const tables: Table[] = [
  {
    name: "users",
    indexes: [
      {
        name: "login",
        field: r.row("login")
      }
    ]
  }
];

const createDatabase = async () => {
  //Create the database if needed.
  const dbList = await r.dbList().run();
  const containsDatabase = dbList.find(db => db === database);

  if (!containsDatabase) {
    try {
      const result = await r.dbCreate(database).run();
      console.log(result);
    } catch (e) {
      console.log("error: ", e);
    }
  }
};

const createTables = async (tables: Table[]) => {
  const tableList = await r.tableList().run();
  for (let table of tables) {
    const containsTable = tableList.find(tb => tb === table.name) !== undefined;
    if (!containsTable) {
      r.tableCreate(table.name).run();
    }
    const indexList = await r
      .table(table.name)
      .indexList()
      .run();
    for (let index of table.indexes) {
      const containsIndex =
        indexList.find(ind => ind === index.name) !== undefined;
      if (!containsIndex) {
        r.table(table.name)
          .indexCreate(index.name, index.field)
          .run();
      }
    }
  }
};

const initialize = async () => {
  await r.connectPool({
    host: "localhost",
    port: 28015,
    user: "admin",
    password: "",
    db: database,
    max: 100,
    buffer: 50
  });

  await createDatabase();
  await createTables(tables);
};

initialize();

export default r;
