export default `
scalar JSON
type UserData {
  cargos: [Cargo]
  cbos: [CBO]
  cnaes: [CNAE]
  empresas: [Empresa]
  setores: [Setor]
}
`;
