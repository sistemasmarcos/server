import * as users from "../../dataAccess/user";
import * as usersData from "../../dataAccess/userData";

export default {
  Query: {
    users: async () => await users.getUsers(),
    user: (_, args) =>
      users.getUser({ login: args.login, password: args.password }),
    getData: async (_, { dataType }, { user }) => {
      return await usersData.getAll(user.userId, dataType);
    }
  },
  Mutation: {
    register: async (_, { input }) => {
      return await users.register(input);
    },
    login: async (_, { login, password }) => {
      return await users.login(login, password);
    },
    dataAdd: async (_, { input }, { user }) => {
      if (!user) return null;
      const retorno = await usersData.add(
        user.userId,
        input.dataType,
        input.payload
      );
      return retorno;
    },
    dataRemove: async (_, { input }, { user }) => {
      if (!user) return null;
      const retorno = await usersData.remove(
        user.userId,
        input.dataType,
        input.payload.id
      );
      return retorno;
    },
    dataUpdate: async (_, { input }, { user }) => {
      if (!user) return null;
      const retorno = await usersData.update(
        user.userId,
        input.dataType,
        input.payload
      );
      return retorno;
    }
  }
};
